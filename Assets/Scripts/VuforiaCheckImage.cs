
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using  UnityEngine.SceneManagement;


public class VuforiaCheckImage : MonoBehaviour
{
    public GameObject vbBtndummy;
    public GameObject dipinto;
    [SerializeField] 
    private string SceneToLoad;


    // Use this for initialization
    void Start()
    {
        vbBtndummy = GameObject.Find("vbBtnDummy");
        dipinto = GameObject.FindGameObjectWithTag("quadro4");

    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        

            SceneManager.LoadScene(SceneToLoad);
            dipinto.SetActive(true);
    }

}