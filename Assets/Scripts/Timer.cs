using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float time;

    public bool TimeIsRunning = false;

    public static int Score;

    private void Start()
    {
        TimeIsRunning = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(TimeIsRunning)
        {
            time += Time.deltaTime;
        }
        else
        {
            TimeIsRunning = false;

            if (time > 0 && time <= 10)
            {
                Score = 3;
                time = 0;
            }
            else
            {
                if (time > 10 && time <= 20)
                {
                    Score = 2;
                    time = 0;
                }
                else
                {
                    if (time > 20 && time <= 30)
                    {
                        Score = 1;
                        time = 0;
                    }
                    else
                    {
                        if (time > 30)
                        {
                            Score = 0;
                            time = 0;
                        }
                    }
                }
            }
        }
    }        
}
