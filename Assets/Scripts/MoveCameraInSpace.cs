using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveCameraInSpace : MonoBehaviour
{
    [SerializeField] private float speed;
    Vector3 hit_position = Vector3.zero;
    Vector3 current_position = Vector3.zero;
    Vector3 camera_position = Vector3.zero;

    // Use this for initialization
    void Start()
    {

    }

    void Update()
    {
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            hit_position = Input.GetTouch(0).position;
            camera_position = transform.position;

        }
        if (Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            current_position = Input.GetTouch(0).position;
            LeftMouseDrag();
        }
    }

    void LeftMouseDrag()
    {

        current_position.z = hit_position.z = camera_position.x;

        // Get direction of movement.  (Note: Don't normalize, the magnitude of change is going to be Vector3.Distance(current_position-hit_position)
        // anyways.  
        Vector3 direction = Camera.main.ScreenToWorldPoint(current_position) - Camera.main.ScreenToWorldPoint(hit_position);

        // Invert direction to that terrain appears to move with the mouse.
        direction = direction * -1;

        Vector3 position = camera_position + direction * speed;

        transform.position = position;

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, 7.91f, 97.47f) ,1.34f, 2.16f );
    }
}