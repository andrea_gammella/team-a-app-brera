using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControlPoke : MonoBehaviour
{
    [SerializeField]
    private GameObject winText;

    // Start is called before the first frame update
    void Start()
    {
        winText.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Grag1.locked && Grag2.locked && Grag3.locked && Grag4.locked && Grag5.locked 
            && Grag6.locked && Grag7.locked && Grag8.locked && Grag9.locked && Grag10.locked && Grag11.locked )
        {
            winText.SetActive(true);
            Inventory.pennelli += Timer.Score;
        }        
    }
}
