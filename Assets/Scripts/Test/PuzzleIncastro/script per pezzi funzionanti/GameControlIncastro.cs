using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControlIncastro : MonoBehaviour
{
    [SerializeField]
    private GameObject winText;
    //
    [SerializeField]
    private GameObject folla1;
    [SerializeField]
    private GameObject folla2;
    //
    [SerializeField]
    private GameObject folla3Inut;
    [SerializeField]
    private GameObject folla4Inut;
    [SerializeField]
    private GameObject folla5Inut;
    //
    [SerializeField]
    private GameObject fondo2Inut;
    [SerializeField]
    private GameObject tizio2Inut;
    [SerializeField]
    private GameObject tizio3Inut;
    [SerializeField]
    private GameObject tizio4Inut;

    //
    [SerializeField]
    private GameObject pendolo;
    [SerializeField]
    private GameObject pendoloInut;

    //
    [SerializeField]
    private GameObject cavaliere;
    [SerializeField]
    private GameObject sagoma;
    //
    [SerializeField]
    private GameObject tizio5Inut;
    [SerializeField]
    private GameObject tizio6Inut;
    [SerializeField]
    private GameObject tizio7Inut;





    // Start is called before the first frame update
    void Start()
    {
        winText.SetActive(false);
        folla1.SetActive(false);
        folla2.SetActive(false);
        winText.SetActive(false);
        folla3Inut.SetActive(false);
        folla4Inut.SetActive(false);
        folla5Inut.SetActive(false);
        tizio2Inut.SetActive(false);
        tizio3Inut.SetActive(false);
        tizio4Inut.SetActive(false);
        tizio5Inut.SetActive(false);
        tizio6Inut.SetActive(false);
        tizio7Inut.SetActive(false);
        pendoloInut.SetActive(false);
        pendolo.SetActive(false);
        cavaliere.SetActive(false);
        sagoma.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Drag1.locked)
        {
            fondo2Inut.SetActive(false);
            folla1.SetActive(true);
            folla2.SetActive(true);
            folla3Inut.SetActive(true);
            folla4Inut.SetActive(true);
            folla5Inut.SetActive(true);
        }

        if (Drag2.locked && Drag3.locked)
        {
            folla3Inut.SetActive(false);
            folla4Inut.SetActive(false);
            folla5Inut.SetActive(false);
            pendolo.SetActive(true);
            pendoloInut.SetActive(true);
        }

        if (Drag4.locked)
        {
            pendoloInut.SetActive(false);
            sagoma.SetActive(true);
            cavaliere.SetActive(true);
            tizio5Inut.SetActive(true);
            tizio6Inut.SetActive(true);
            tizio7Inut.SetActive(true);
        }

        if (Drag5.locked && Drag6.locked)
        {
            tizio5Inut.SetActive(false);
            tizio6Inut.SetActive(false);
            tizio7Inut.SetActive(false);
        }


        if (Drag1.locked && Drag2.locked && Drag3.locked && Drag4.locked && Drag5.locked && Drag6.locked )
        {
            winText.SetActive(true);
        }
    }
}
