using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuadriTouch : MonoBehaviour
{
    [SerializeField] private string SceneToLoad;
    private GameObject cam;
    private Ray r;

    void Start()
    {
        
    }

    private void Update()
    {

        if (Input.GetTouch(0).phase > 0)
        {
            var h = Physics2D.Raycast(cam.transform.position, cam.transform.forward);

            if (!h)
            {
                Debug.Log($"colpito{h.collider.gameObject.name}");
                if (h.collider.gameObject == gameObject)
                {
                    SceneManager.LoadScene(SceneToLoad);
                }
            }

        }
    }
}
