using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayTrace : MonoBehaviour
{
    public Transform hitPoint;
    public float distance = 10f;
    public GameObject selectedObject;
    RaycastHit hit;
    Vector3 offset;
    private void ObjectInteraction()
    {
        if (Input.GetMouseButtonUp(0))
        {
            selectedObject = null;
        }
        int layerMask = ~(1 << 2);
        if (Input.GetMouseButton(0))
        {
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, distance, layerMask))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.green);
                if (hit.collider != null)
                {
                    if (hit.collider.GetComponent<Rigidbody>())
                    {
                        Debug.Log("This object has a rigidbody.");
                        if (selectedObject == null)
                        {
                            hitPoint.transform.position = hit.point;
                            selectedObject = hit.collider.gameObject;
                            offset = selectedObject.transform.position - hit.point;
                        }
                    }
                }
            }
            if (selectedObject != null)
            {
                selectedObject.transform.position = hitPoint.transform.position + offset;
            }
        }
    }

    public void RotateObject()
    {
        hit.transform.Rotate(0, 90, 0);
    }
}
