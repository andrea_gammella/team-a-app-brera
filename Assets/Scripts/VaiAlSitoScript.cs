using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VaiAlSitoScript : MonoBehaviour
{
    [SerializeField] private string SceneToLoad;
    public void LoadSite()
    {
        SceneManager.LoadScene(SceneToLoad);
    }
}
