using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcceptanceRadius : MonoBehaviour
{
    [SerializeField] 
    float Range;

    public static bool trovato =false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if ((transform.eulerAngles.x < Range || transform.eulerAngles.x > (360 - Range)) && transform.eulerAngles.y < (90 + Range) && transform.eulerAngles.y > (90 - Range))
        {

            trovato = true;
            Inventory.pennelli += Timer.Score;
        }
    }
}
