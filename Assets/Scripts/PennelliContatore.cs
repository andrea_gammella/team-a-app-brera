using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PennelliContatore : MonoBehaviour
{
    public Text counterText;

    public void Update()
    {
        counterText.text = Inventory.pennelli.ToString();

    }
}
